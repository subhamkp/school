var mongoose = require('mongoose');

mongoose.connect('mongodb://skp:skp@28@ds163918.mlab.com:63918/school');

// Get Mongoose to use the global promise library
mongoose.Promise = global.Promise

var db = mongoose.connection;

// User Schema
var UserSchema = mongoose.Schema({
    username: {
        type: String,
        index: true
    },
    password: {
        type: String,
        required: true,
    },
    email: {
        type: String
    },
    name: {
        type: String
    },
    profileimage: {
        type: String
    }
});

var User = module.exports = mongoose.model('User', UserSchema);

module.exports.createUser = function(newUser, callback) {
    // Create user
    newUser.save(callback);
};
