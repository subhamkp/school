const express = require('express');
const multer= require('multer');
const router = express.Router();
const path = require('path');

var User = require('../models/user');

// Set The Storage Engine
const storage = multer.diskStorage({
  destination: './uploads/',
  filename: function(req, file, cb){
    cb(null,file.fieldname + '-' + Date.now() + path.extname(file.originalname)); // first parameter is error, 2nd file name
  } // path.extname() gives extensions
});

// Init Upload
const upload = multer({
  storage: storage,
  limits:{fileSize: 1000000},
  fileFilter: function(req, file, cb){
    checkFileType(file, cb);
  }
}).single('profileimage');

// Check File Type
function checkFileType(file, cb){
  // Allowed ext
  const filetypes = /jpeg|jpg|png|gif/;
  // Check ext
  const extname = filetypes.test(path.extname(file.originalname).toLowerCase());
  // Check mime
  const mimetype = filetypes.test(file.mimetype);

  if(mimetype && extname){
    return cb(null,true);
  } else {
    cb('Error: Images Only!');
  }
}

/* GET users listing. */
router.get('/', function(req, res, next) {
  res.send('respond with a resource');
});

router.get('/register', function(req, res, next) {
  res.render('register',{
    'title':'Register'
  });
});

router.get('/login', function(req, res, next) {
  res.render('login',{
    'title':'Login'
  });
});

router.post('/register',(req,res,next)=>{
  var name=req.body.name;
  var email=req.body.email;
  var username=req.body.username;
  var password=req.body.password;
  var password2=req.body.password2;

  upload(req, res, (err) => {
    if(err){
      console.log(err);
    } else {
      if(req.file == undefined){
          console.log('Error: No File Selected!');
          //Set a default profileImage
          var profileImageName='noimage.png';
      } else {
        //file info
        var profileImageOriginalName=req.file.originalname;
        var profileImageName=req.file.filename;
        var profileImageMime=req.file.mimetype;
        var profileImagePath=req.file.path;
        var profileImageExt=path.extname(profileImageOriginalName);
        var profileImageSize=req.file.size;
        console.log({
          msg: 'File Uploaded!',
          file: `uploads/${profileImageName}`,
          profileImageExt
        });
      }
    }
  });




    //form validation
    req.checkBody('name','Name field is required').notEmpty();
    req.checkBody('email','Email field is required').notEmpty();
    req.checkBody('email','Email not valid').isEmail();
    req.checkBody('username','Username field is required').notEmpty();
    req.checkBody('password','Password field is required').notEmpty();
    req.checkBody('password2','Password do not match').equals(req.body.password);

    //check for errors
    var errors=req.validationErrors();

    if(errors){
      res.render('register',{
        errors, title:'Register', name, email, username, password,password2  //concise object literals
      });
    } else{
      var newUser =new User({
        name,email,username,password,
        profileimage:profileImageName
      });

      //create users
      User.createUser(newUser,(err,user)=>{
        if(err) throw err;
        console.log(err);
      });

      // success message
      req.flash('success',"you are now registered and may login");
      res.location('/');
      res.redirect('/');
    }
});


module.exports = router;
